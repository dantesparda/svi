/* Date: 25/10/2012
 * SVI - Simulation of VI        by Dante Sparda
 */

/*
 * The main() function of Svi.
 */

#include "common.h"

int main(int argc, char **argv)
{
   enum Mode mode = NORMAL;
   FILE *fp = NULL;
   int ch;
   int i;
   Information infor = {
     NULL, NULL, 0, 0, 0, 0, NULL, 0
   };
   
   
   InitialWin();
   getmaxyx(stdscr, infor.maxy, infor.maxx);
   infor.x = infor.y = 0;
   
   if(argc == 2) {
	
     infor.filename = argv[1];
     LoadFile(&infor);
     Display(infor);
     move(infor.y, infor.x);
   } else
     infor.head = infor.cur = CreatLine(NULL, NULL, infor.maxx);
   
   while(1) {
     switch(mode) {
     case INSERT:
       mode = Insert(&infor);
       break;
     case NORMAL:
       mode = Normal(&infor);
       break;
     }
   }
   
   return -1;
}
