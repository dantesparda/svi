/* Date: 25/08/2012
 * SVI - Simulation of VI         by Dante Sparda
 */

/* 
 * This file contains the dedinitions of structures that are used by Svi
 */

typedef struct m1
{
  struct m1 *pre;
  struct m1 *next;
  char *s;
  int l;
} Line;

enum Mode
{
  NORMAL, INSERT
};

typedef struct
{
  Line *head;
  Line *cur;
  int x, y;
  int maxx, maxy;
  char *filename;
  int saved;
} Information;
