/* Date: 25/10/2012
 * SVI - Simulation of VI        by Dante Sparda
 */

/*
 * This file contains the necessary head files to be included
 * and defines of some const vars and functions' headers.
 */

#include <unistd.h>
#include <curses.h>
#include <malloc.h>
#include <stdio.h>
#include <string.h>
#include "structs.h"
#include <stdlib.h>

#define ESC 27
#define ENTER '\n'

void InitialWin ();
Line* CreatLine (Line *pr, Line *ne, int l);
void SaveFile (Information *infor);
void LoadFile (Information *infor);
void Display (Information infor);
enum Mode Normal (Information *infof);
enum Mode Insert (Information *infor);
void Command (Information *infor);
void OverLine (Line *cur);
void DeLine (Line *cur, int y, int maxx);
void OvLine (Line *cur, char ch, int y, int maxx);
