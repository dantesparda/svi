/* Date: 25/10/2012
 * SVI - Simulation of VI        by Dante Sparda
 */

/*
 * This file contains the definitions of fuctions that used in mode Normal.
 */

#include "common.h"

enum Mode Normal (Information *infor)
{
  int ch;

  move(infor->maxy-1, 0);
  printw("--NORMAL--");
  move(infor->y, infor->x);
  refresh();
	
  while(1) {
    ch = getch();
    switch(ch) {
    case KEY_UP:
      if(infor->y > 0 && infor->cur->pre != NULL) {
	infor->cur = infor->cur->pre;
	if(infor->x > infor->cur->l+1)
	  infor->x = infor->cur->l+1;
	move(--(infor->y), infor->x);
	refresh();
      }
      break;

    case KEY_DOWN:
      if(infor->y < infor->maxy-2 && infor->cur->next != NULL) {
	infor->cur = infor->cur->next;
	if(infor->x > infor->cur->l+1)
	  infor->x = infor->cur->l+1;
	move(++(infor->y), infor->x);
	refresh();
      }
      break;
		
    case KEY_LEFT:
      if(infor->x > 0) {
	move(infor->y, --(infor->x));
	refresh();
      }
      break;
			
    case KEY_RIGHT:
      if(infor->x < infor->maxx && infor->x < infor->cur->l+1) {
	move(infor->y, ++(infor->x));
	refresh();
      }
      break;
      
    case 'i':
      return INSERT;
      
    case ':':
      Command(infor);
      move(infor->y, infor->x);
      refresh();
      break;
    }
  }
}

void Command (Information *infor)
{
  char buf[10];
  int ch, i = -1;
  int y = infor->maxy-1;

  move(y, 0);
  addstr(":\n");
  refresh();
	
  while(1) {
    ch = getch();
    switch(ch) {
    case KEY_BACKSPACE:
      if(i >= 0) {
	i--;
	move(y, i+2);
	delch();
	refresh();
      } else if(i == -1) {
	move(y, 0);
	delch();
	refresh();
	return;
      }
      break;
			
    case ENTER:
      if(strcmp(buf, "w") == 0) {
	SaveFile(infor);
	infor->saved = 1;
	move(y, 0);
	printw("File Has Been Saved.\n");
	refresh();
	return;
      }
			
      if(strcmp(buf, "q") == 0) {
	if(!infor->saved) {
	  move(y, 0);
	  printw("File Has Not Been Saved.\n");
	  refresh();
	  return;
	}
	endwin();
	exit(0);
      }
			
      if(strcmp(buf, "wq") == 0) {
	SaveFile(infor);
	endwin();
	exit(0);
      }
			
      if(strcmp(buf, "q!") == 0) {
	endwin();
	exit(0);
      }
			
      move(y, 0);
      printw("Unknown Command.\n");
      refresh();
      return;
			
    default:
      if(!isgraph(ch))
	break;
      if(i < 9) {
	buf[++i] = ch;
	addch(ch);
	refresh();
      }
    }
  }
}
