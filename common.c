/* Date: 25/10/2012
 * SVI - Simulation of VI        by Dante Sparda
 */

/*
 * This file contains some common functions.
 */

#include "common.h"

void InitialWin ()
{
  initscr();
  cbreak();
  noecho();
  keypad(stdscr, TRUE);
}

void SaveFile (Information *infor)
{
  FILE *fp = NULL;
  char *filename;
  Line *temp = NULL;
   
  if(infor->filename == NULL) {
      move(infor->maxy-1, 0);
      printw("Please Enter A Filename: ");
      echo();
      nocbreak();
      filename = (char*)malloc(sizeof(char)*10);
      getstr(filename);
      infor->filename = filename;
      noecho();
      cbreak();
  }
   
  fp = fopen(infor->filename, "w");
  temp = infor->head;
  while(temp) {
    if(temp->l == -1)
      fprintf(fp, "\n");
    else
      fprintf(fp, "%s\n", temp->s);
    temp = temp->next;
  }
  fclose(fp);
   
  return;
}

void LoadFile (Information *infor)
{
  FILE *fp = NULL;
  char buff;
  int i = 0;
  Line *temp = NULL;
   
  fp = fopen(infor->filename, "r");
  infor->head = infor->cur = CreatLine(NULL, NULL, infor->maxx);
  if(fp == NULL)
    return;
   
  temp = infor->head;
  while(fscanf(fp, "%c", &buff) != EOF) {
    if(buff == '\n') {
      temp->s[temp->l+1] = '\0';
      temp = CreatLine(temp, temp->next, infor->maxx);
      i = 0;
      continue;
    }
	
    if(i == 0) {
      temp->s[0] = buff;
      i++;
      temp->l++;
      continue;
    }
    
    if(i % (infor->maxx-1) == 0) {
      temp->l++;
      temp->s[infor->maxx-1] = buff;
      temp->s[infor->maxx] = '\0';
      temp = CreatLine(temp, temp->next, infor->maxx);
      i = 0;
      continue;
    }
	
    temp->s[i] = buff;
    temp->l++;
    i++;
  }
   
   if(temp->pre != NULL) {
     temp->pre->next = NULL;
     free(temp);
   }
   
   infor->cur = infor->head;
   
   return;
}

void Display (Information infor)
{
  Line *temp = NULL;
  int x, y;
   
  temp = infor.head;
  y = infor.y;
  x = infor.x;
   
  while(temp) {
    move(y, x);
    addstr(temp->s);
    y++;
    x = 0;
    temp = temp->next;
  }
  refresh();
   
  return;
}

Line* CreatLine(Line *pr, Line *ne, int l)
{
   
  Line *temp = NULL;
   
  temp = (Line*)malloc(sizeof(Line));
  if(pr)
    pr->next = temp;
  temp->pre = pr;
  temp->next = ne;
  if(ne) {
    ne->pre = temp;
  }
  temp->l = -1;
  temp->s = (char*)malloc(sizeof(char)*(l+1));
  temp->s[0] = '\0';
   
  return temp;
}

