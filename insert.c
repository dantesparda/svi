/* Date: 25/10/2012
 * SVI - Simulation of VI        by Dante Sparda
 */

/*
 * This file contains the definitions of functions that used in mode Insert.
 */

#include "common.h"

enum Mode Insert (Information *infor)
{
  int ch;
  int i, t;
  Line *temp;

  move(infor->maxy-1, 0);
  printw("--INSERT--");
  move(infor->y, infor->x);
  infor->saved = 0;
  refresh();
	
  while(1) {
    ch = getch();
    switch(ch) {
    case KEY_UP:
      if(infor->y > 0 && infor->cur->pre != NULL) {
	infor->cur = infor->cur->pre;
	if(infor->x > infor->cur->l+1)
	  infor->x = infor->cur->l+1;
	move(--(infor->y), infor->x);
	refresh();
      }
      break;
      
    case KEY_DOWN:
      if(infor->y < infor->maxy-2 && infor->cur->next != NULL) {
	infor->cur = infor->cur->next;
	if(infor->x > infor->cur->l+1)
	  infor->x = infor->cur->l+1;
	move(++(infor->y), infor->x);
	refresh();
      }
      break;
			
    case KEY_LEFT:
      if(infor->x > 0) {
	move(infor->y, --(infor->x));
	refresh();
      }else if(infor->cur->pre != NULL){
	  	infor->cur = infor->cur->pre;		
	  	infor->y--;
	  	infor->x = infor->cur->l+1;
	  	move(infor->y, infor->x);
	  	refresh();
	  }
      break;
			
    case KEY_RIGHT:
      if(infor->x < infor->cur->l+1) {
	if(infor->x < infor->maxx-1)
	  infor->x++;
	else if(infor->y < infor->maxy-2 && infor->cur->next != NULL) {
	  infor->y++;
	  infor->x = 0;
	  infor->cur = infor->cur->next;
	}
	move(infor->y, infor->x);
	refresh();
      }
      break;
			
    case KEY_BACKSPACE:
      if(infor->x > 0) {
	for(i = --(infor->x); i < infor->cur->l; i++)
	  infor->cur->s[i] = infor->cur->s[i+1];
	infor->cur->l--;
	infor->cur->s[ infor->cur->l+1 ] = '\0';
	move(infor->y, infor->x);
	delch();
	refresh();
      } else if(infor->cur->l == -1){
	if(!infor->cur->pre)
	  break;
	deleteln();
	temp = infor->cur;
	infor->cur = temp->pre;
	infor->cur->next = temp->next;
	if(temp->next)
	  temp->next->pre = infor->cur;
	free(temp->s);
	free(temp);
	if(infor->cur->l == infor->maxx-1) {
	  infor->cur->l--;
	  infor->cur->s[infor->cur->l] = '\0';
	}
	move(--(infor->y), infor->x = infor->cur->l+1);
	delch();
	refresh();
      } else {
	if(!infor->cur->pre)
	  break;
	infor->cur = infor->cur->pre;
	if(infor->cur->l == infor->maxx-1) {
	  infor->cur->l--;
	  infor->cur->s[infor->maxx-1] = '\0';
	  move(infor->y-1, infor->maxx-1);
	  delch();
	  refresh();
	}
	infor->x = infor->cur->l+1;
       	DeLine(infor->cur->next, infor->y, infor->maxx);
	move(--(infor->y), infor->x);
	refresh();
      }
      break;
			
    case ESC:
      return NORMAL;
			
    case ENTER:
      if(infor->x == infor->cur->l+1) {
	infor->cur = CreatLine(infor->cur, infor->cur->next, infor->maxx);
	move(++(infor->y), infor->x = 0);
	insertln();
	move(infor->y, infor->x);
	refresh();
      } else {
	temp = infor->cur;
	infor->cur = CreatLine(infor->cur, infor->cur->next, infor->maxx);
	addch('\n');
	strcpy(infor->cur->s, &(temp->s[infor->x]));
	infor->cur->l = temp->l - infor->x;
	temp->l = infor->x-1;
	move(++(infor->y), 0);
	insertln();
	move(infor->y, 0);
	addstr(infor->cur->s);
	infor->x = 0;
	move(infor->y, infor->x);
	refresh();
      }
      break;
			
    default:
      if(!isprint(ch))
      	break;
      if(infor->cur->l < infor->maxx-1) {
	for(i = infor->cur->l+1; i > infor->x; i--)
	  infor->cur->s[i] = infor->cur->s[i-1];
	infor->cur->s[++(infor->cur->l)+1] = '\0';
	infor->cur->s[infor->x] = ch;
	insch(ch);
	if(infor->x+1 > infor->maxx-1) {
	  infor->cur = CreatLine(infor->cur, infor->cur->next, infor->maxx);
	  infor->y++;
	  infor->x = 0;
	  move(infor->y, infor->x);
	  insertln();
	} else
	  infor->x++;
	move(infor->y, infor->x);
      } else {
	insch(ch);
	t = infor->cur->s[infor->maxx-1];
	for(i = infor->maxx-1; i > infor->x; i--)
	  infor->cur->s[i] = infor->cur->s[i-1];
	infor->cur->s[i] = ch;
	move(infor->y, infor->x++);
	refresh();
	if(!infor->cur->next)
	  infor->cur->next = CreatLine(infor->cur, NULL, infor->maxx);
	OvLine(infor->cur->next, t, infor->y+1, infor->maxx);
	if(infor->x > infor->maxx-1) {
	  infor->y++;
	  infor->x = 0;
	  infor->cur = infor->cur->next;
	}
	move(infor->y, infor->x);
	refresh();
      }	
    }
  }
}

void DeLine(Line *cur, int y, int maxx)
{
  int n = 0,k=0;
  int mode = 0;

  if(!cur)
    return;

  n = maxx-1 - cur->pre->l;
  if(cur->l == maxx-1)
    mode = 1;

  strncpy(&(cur->pre->s[cur->pre->l+1]), cur->s, n);
  move(y-1, cur->pre->l+1);
  addstr(&(cur->pre->s[cur->pre->l+1]));
  refresh();

  if(mode == 1) {
    cur->pre->l = maxx-1;
    cur->l -= n;
    
    // strcpy(cur->s, &(cur->s[n]));
    for( k = 0; cur->s[k] != '\0'; k++ )
      cur->s[k] = cur->s[k+n];

    cur->s[cur->l+1] = '\0';
    move(y, cur->l+1);
    addch('\n');
    refresh();

    DeLine(cur->next, y+1, maxx);
  } else {
    if(cur->l >= n) {
      cur->pre->l = maxx-1;
      cur->l -= n;

      // strcpy(cur->s, &(cur->s[n]));
      for( k = 0; cur->s[k] != '\0'; k++ )
	cur->s[k] = cur->s[k+n];

      cur->s[cur->l+1] = '\0';
      move(y, 0);
      printw("%s\n",cur->s);
      refresh();
    } else {
      cur->pre->l += cur->l+1;
      move(y, 0);
      deleteln();
      refresh();

      cur->pre->next = cur->next;
      if(cur->next) 
	cur->next->pre = cur->pre;
      free(cur->s);
      free(cur);
    }
  }
}

void OvLine(Line *cur, char ch, int y, int maxx)
{
  int i, temp;

  if(!cur)
    return;

  if(cur->l != -1) {
    temp = cur->s[cur->l];
    for(i = cur->l; i > 0; i--)
      cur->s[i] = cur->s[i-1];
    cur->s[0] = ch;
    move(y, 0);
    insch(ch);
    refresh();
    
    if(cur->l == maxx-1) {
      if(!cur->next)
	cur->next = CreatLine(cur, NULL, maxx);
      OvLine(cur->next, temp, y+1, maxx);
    } else {
      cur->l++;
      cur->s[cur->l] = temp;
      cur->s[cur->l+1] = '\0';
    }
  } else {
    cur->l++;
    cur->s[cur->l] = ch;
    cur->s[cur->l+1] = '\0';
    move(y, 0);
    addch(ch);
    refresh();
  }
}
